using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<string> getVehicles()
        {
            string result = JsonConvert.SerializeObject(_parkingService.GetVehicles());
            return result;
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<string> getVehicleById(string id)
        {
            try
            {
                Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                    RegexOptions.Compiled);
                if (!regexIdPattern.IsMatch(id))
                {
                    return BadRequest();
                }
                string result =
                    JsonConvert.SerializeObject(_parkingService.GetVehicles().ToList().Find(v => v.Id == id));
                return Ok(result);
            }
            catch (ArgumentException ex)
            {
                
                    return NotFound();
                
            }
        }

        [HttpPost]
        public ActionResult addVehicle(Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
                return Created(new Uri("api/{controler}", UriKind.Relative)
                    , vehicle);
            }
            catch
            {
                return BadRequest();
            }
        }
        
        [Route("{id}")]
        [HttpDelete]
        public ActionResult removeVehicle(string id)
        {
            try
            {
                Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                    RegexOptions.Compiled);
                if (!regexIdPattern.IsMatch(id))
                {
                    return BadRequest();
                }
                string result =
                    JsonConvert.SerializeObject(_parkingService.GetVehicles().ToList().Find(v => v.Id == id));
                
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException ex)
            {
                
                    return NotFound();
                
            }
        }
    }
}