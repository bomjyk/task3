﻿using System;
using System.Timers;
using CoolParking.BL.Services;
using CoolParking.BL.Models;


namespace CoolParking.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            LogService logger = new LogService("log.txt");
            TimerService loggerTimer = new TimerService(new Timer(60000));
            TimerService withdrawnTimer = new TimerService(new Timer(5000));
            ParkingService parkingService = new ParkingService(withdrawnTimer, loggerTimer, logger);
            bool menuAlive = true;
            Menu menu = new Menu(logger, loggerTimer, withdrawnTimer, parkingService);
            System.Console.Clear();
            menu.PrintWelcomeMessage();
            while (menu.IsAlive)
            {
                try
                {
                    System.Console.Clear();
                    menu.InteractiveMenu();
                    System.Console.Clear();
                }
                catch (ArgumentException ex)
                {
                    System.Console.Clear();
                    System.Console.WriteLine(ex.Message);
                    System.Console.ReadKey();
                }
                catch (Exception ex)
                {
                    System.Console.Clear();
                    System.Console.WriteLine(ex.Message);
                    System.Console.ReadKey();
                }
            }
        }
    }
}