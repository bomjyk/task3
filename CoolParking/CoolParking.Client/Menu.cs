using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Channels;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;


namespace CoolParking.Client
{
    public class Menu
    {
        private Dictionary<int, Action> Choices;
        private ILogService _logService;
        private ITimerService _logTimerService, _timerService;
        private ParkingService _parkingService;
        public  bool IsAlive { get; set; }
        
        HttpClient client;
        public Menu(ILogService logService, ITimerService logTimerService, ITimerService timerService,
            ParkingService parkingService)
        {
            _logService = logService;
            _logTimerService = logTimerService;
            _timerService = timerService;
            _parkingService = parkingService;
            Choices = new Dictionary<int, Action>()
            {
                {1, PrintCurrentBalance},
                {2, PrintEarnedMoney},
                {3, PrintFreeSpaces},
                {4, PrintAllCurrentTransactions},
                {5, PrintTransactionHistory},
                {6, AddVehicleToParking},
                {7, RemoveVehicleFromParking},
                {8, TopUpVehicle},
                {9, Exit}
            };
            IsAlive = true;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            client = new HttpClient(clientHandler);
        }
        public void PrintWelcomeMessage()
        {
            System.Console.WriteLine("Hello. This is application for parking director.\n" +
                                     "It's made for automatizing routine tasks with big vehicle parking service.\n" +
                                     "To get started choose menu options.");
        }
        public void InteractiveMenu()
        {
            System.Console.WriteLine("Menu:");
            foreach (var choice in Choices)
            {
                System.Console.WriteLine($"{choice.Key.ToString(), -3} : {choice.Value.Method.Name.ToString()}");
            }

            int userChoice;
            bool parsingIsOk = Int32.TryParse(System.Console.ReadLine(), out userChoice);
            if (!parsingIsOk)
            {
                throw new ArgumentException("You wrote in incorrect format. Try to write only number.");
            }

            if (userChoice > Choices.Count)
            {
                throw new ArgumentException("There is no such choice. Try to write number from the menu.");
            }
            Choices[userChoice]?.Invoke();
        }

        private void PrintCurrentBalance()
        {
            System.Console.Clear();
            System.Console.Write("Curring parking balance: ");
            var result = client.GetStringAsync("http://localhost:5000/api/Parking/balance");
            System.Console.WriteLine(result.Result);
            System.Console.ReadKey();
        }

        private void PrintEarnedMoney()
        {
            var transactions = JsonConvert.DeserializeObject<List<TransactionInfo>>(
                client.GetStringAsync("http://localhost:5000/api/transactions/last").Result);
            decimal earnedMoney = transactions.Sum((tr) => tr.Sum );
            System.Console.Clear();
            System.Console.Write("Earned money: ");
            System.Console.WriteLine(earnedMoney);
            System.Console.ReadKey();
        }

        private void PrintFreeSpaces()
        {
            string freeSpaces = client.GetStringAsync("http://localhost:5000/api/Parking/freePlaces").Result;
            System.Console.Clear();
            System.Console.Write("Free spaces: ");
            System.Console.WriteLine($"{freeSpaces.ToString()} from {Settings.ParkingCapacity.ToString()}");
            System.Console.ReadKey();
        }

        private void PrintAllCurrentTransactions()
        {
            var transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(
                client.GetStringAsync("http://localhost:5000/api/transactions/last").Result);
            System.Console.Clear();
            if (transactions.Length == 0)
            {
                System.Console.WriteLine("There is no applied transactions yet.");
            }
            else {
                System.Console.WriteLine("All current transactions: ");
                foreach (var transaction in transactions)
                {
                    System.Console.WriteLine($"{transaction.VehicleId} - {transaction.TimeStamp.ToString()}" +
                                             $" - {transaction.Sum.ToString()}");
                }
            }
            System.Console.ReadKey();
        }

        private void PrintTransactionHistory()
        {
            string transactionHistory = client.GetStringAsync("http://localhost:5000/api/transactions/all").Result;
            string[] transactions = transactionHistory.Split('\n');
            System.Console.Clear();
            System.Console.WriteLine("Transaction history: ");
            foreach (var transaction in transactions)
            {
                System.Console.WriteLine(transaction);
            }
            System.Console.ReadKey();
        }

        private void AddVehicleToParking()
        {
            System.Console.Clear();
            System.Console.WriteLine("Write vehicle Id in format (XX-YYYY-XX) where X - capitalized alphabetic character" +
                                     " Y - number from 0 to 9");
            string userInputId = System.Console.ReadLine();
            Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                RegexOptions.Compiled);
            if (!regexIdPattern.IsMatch(userInputId))
            {
                throw new ArgumentException("Incorrect format for id. Try again");
            }
            System.Console.Clear();
            if (this._parkingService.GetVehicles().ToList().FindIndex
                ( veh => veh.Id == userInputId) >= 0)
            {
                throw new ArgumentException("There are already added car with such id.");
            }
            System.Console.WriteLine("Choose vehicle type (available types):");
            int counter = 0;
            foreach (var type in  Enum.GetValues((typeof(VehicleType))))
            {
                System.Console.WriteLine($"{counter.ToString()} - {type.ToString()}");
                counter++;
            }

            int vehicleType;
            bool parsingIsOk = Int32.TryParse(System.Console.ReadLine(), out vehicleType);
            if (!parsingIsOk)
            {
                throw new ArgumentException("You wrote in incorrect format. Try to write only number.");
            }

            if (vehicleType > Enum.GetValues(typeof(VehicleType)).Length)
            {
                throw new ArgumentException("There is no such choice. Try to write number from the menu.");
            }

            VehicleType userInputType = (VehicleType) vehicleType;
            System.Console.Clear();
            System.Console.WriteLine("Write initial vehicle balance:");
            decimal userInputBalance;
            parsingIsOk = Decimal.TryParse(System.Console.ReadLine(), out userInputBalance);
            if (!parsingIsOk)
            {
                throw new ArgumentException("You wrote in incorrect format. Try to write only number.");
            }

            if (userInputBalance < 0)
            {
                throw new ArgumentException("Initial balance can not be negative number.");
            }

            Vehicle vehicle = new Vehicle(userInputId, userInputType, userInputBalance);
            var json = JsonConvert.SerializeObject(vehicle);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var result = client.PostAsync("http://localhost:5000/api/vehicles/", stringContent).Result;
            System.Console.Clear();
            System.Console.WriteLine(result);
            System.Console.ReadKey();
        }

        private void RemoveVehicleFromParking()
        {
            System.Console.Clear();
            System.Console.WriteLine("Current vehicle list:");
            var resultString = client.GetStringAsync("http://localhost:5000/api/vehicles");
            var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(resultString.Result);
            foreach (var vehicle in vehicles)
            {
                System.Console.WriteLine($"{vehicle.Id} - {Enum.GetName(vehicle.VehicleType)} - {vehicle.Balance.ToString()}");
            }
            System.Console.WriteLine("Write vehicle id that you want to remove.");
            string userInputId = System.Console.ReadLine();
            Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                RegexOptions.Compiled);
            if (!regexIdPattern.IsMatch(userInputId))
            {
                throw new ArgumentException("Incorrect format for id. Try again");
            }
            client.DeleteAsync("http://localhost:5000/api/vehicles/" + userInputId);
        }

        private void TopUpVehicle()
        {
            System.Console.Clear();
            System.Console.WriteLine("Current vehicle list:");
            var resultString = client.GetStringAsync("http://localhost:5000/api/vehicles");
            var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(resultString.Result);
            foreach (var vehicle in vehicles)
            {
                System.Console.WriteLine($"{vehicle.Id} - {Enum.GetName(vehicle.VehicleType)} - {vehicle.Balance.ToString()}");
            }
            System.Console.WriteLine("Write vehicle id that you want to top up.");
            string userInputId = System.Console.ReadLine();
            Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                RegexOptions.Compiled);
            if (!regexIdPattern.IsMatch(userInputId))
            {
                throw new ArgumentException("Incorrect format for id. Try again");
            }
            if (vehicles.FindIndex
                ( veh => veh.Id == userInputId) < 0)
            {
                throw new ArgumentException("There are no vehicle with such id.");
            }
            System.Console.WriteLine("Write sum");
            decimal userInputSum;
            bool parsingIsOk = Decimal.TryParse(System.Console.ReadLine(), out userInputSum);
            if (!parsingIsOk)
            {
                throw new ArgumentException("You wrote in incorrect format. Try to write only numbers.");
            }

            var json = $"id={userInputId}&Sum={userInputSum.ToString()}";
            var stringContent = new StringContent(json, Encoding.Default, "plain/text");
            var result = client.PutAsync("http://localhost:5000/api/Transactions/TopUpVehicle",stringContent).Result;
            Console.WriteLine(result);
            Console.ReadLine();
        }

        private void Exit()
        {
            IsAlive = false;
        }
    }
}