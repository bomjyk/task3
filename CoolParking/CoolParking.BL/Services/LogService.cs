﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = File.AppendText(LogPath))
            {
                streamWriter.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("There is no such file.");
            }

            using (StreamReader streamReader = File.OpenText(LogPath))
            {
                string logInformation = streamReader.ReadToEnd();
                return logInformation;
            }
        }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }
    }
}