using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        
        [Route("last")]
        [HttpGet]
        public ActionResult<TransactionInfo[]> getLastTransaction()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }
        [Route("all")]
        [HttpGet]
        public ActionResult<string> getAllTransactions()
        {
            try
            {
                string transactions = _parkingService.ReadFromLog();
                return Ok(transactions);
                
            }
            catch
            {
                return NotFound();
            }
        }

        [Route("topUpVehicle")]
        [HttpPut]
        public ActionResult<Vehicle> topUpVehicle(string id, decimal Sum)
        {
            Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                RegexOptions.Compiled);
            if (!regexIdPattern.IsMatch(id))
            {
                return BadRequest();
            }
            Vehicle? vehicle =  _parkingService.GetVehicles().ToList().Find(v => v.Id == id);
            if (vehicle == null)
            {
                return NotFound();
            }
            _parkingService.TopUpVehicle(id,Sum);
            return Ok(_parkingService.GetVehicles().ToList().Find(v => v.Id == id));
        }
    }
}