﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
// TODO: make all requirements
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _parking;
        private readonly int _capacity;
        public List<Vehicle> Vehicles { get; set; }
        public int Capacity
        {
            get { return _capacity; }
        }
        public decimal Balance { get; set; }

        private Parking()
        {
            Balance = Settings.InitialParkingBalance;
            _capacity = Settings.ParkingCapacity;
            Vehicles = new List<Vehicle>();
        }

        public static Parking GetParking()
        {
            if (_parking == null)
            {
                _parking = new Parking();
            }

            return _parking;
        }
    }
}