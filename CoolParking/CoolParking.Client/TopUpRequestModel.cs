using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace CoolParking.Client
{
    public class TopUpRequestModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }

        public TopUpRequestModel(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}