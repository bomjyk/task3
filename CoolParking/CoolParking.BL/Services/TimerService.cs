﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        public void Start()
        {
            _timer.Elapsed += this.Elapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        public TimerService(Timer timer)
        {
            _timer = timer;
            _timer.AutoReset = true;
        }
    }
}