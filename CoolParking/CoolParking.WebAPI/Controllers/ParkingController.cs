using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("balance")]
        [HttpGet]
        public ActionResult<decimal> getBalance()
        {
            decimal result = _parkingService.GetBalance();
            return result;
        }

        [Route("capacity")]
        [HttpGet]
        public ActionResult<decimal> getCapacity()
        {
            decimal result = _parkingService.GetCapacity();
            return result;
        }

        [Route("freePlaces")]
        [HttpGet]
        public ActionResult<int> getFreePlaces()
        {
            int result = _parkingService.GetFreePlaces();
            return result;
        }
    }
}